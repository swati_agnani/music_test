import React, { Component } from 'react';
import './Footer.css';
import { Link } from 'react-router-dom';
// import {Alert} from 'react-bootstrap';
// import {cookie} from 'react-cookie';

export default class Footer extends Component {

  render() {
    // alert(this.state.check);
    return (
      <React.Fragment>
        <div className="footer">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <div className="footer__inerrow">
                  <ul>
                    <li>
                      <Link to="/">
                        Home
                      </Link>
                    </li>
                    <li>
                      <Link to="#">
                        About Us
                      </Link>
                    </li>
                    <li>
                      <Link to="#">
                        Privacy Policy
                      </Link>
                    </li>
                    <li>
                      <Link to="#">
                          Contact Us
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-12">
                <div className="footer__bottom">
                    <Link to="/" className="ffhom__logo">
                        {/* <img src="images/footer-logo.png" alt="Footer-Logo" /> */}
                        SONG LIBRARY
                    </Link>
                  <div className="copyright__sec">
                    <p>2019 &copy; All Right Reserved By <a href="#" target="_blank">Auriga IT</a></p>
                  </div>
                  <div className="footer__socialmedia">
                    <ul>
                      <li>
                        <a href="" target="_blank">
                          <i className="fab fa-facebook-square"></i>
                        </a>
                      </li>
                      <li>
                        <a href="" target="_blank">
                          <i className="fab fa-twitter-square"></i>
                        </a>
                      </li>
                      <li>
                        <a href="" target="_blank">
                          <i className="fab fa-linkedin"></i>
                        </a>
                      </li>
                      <li>
                        <a href="" target="_blank">
                          <i className="fab fa-instagram"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* <Alert dismissible variant="danger" className="verify__textbox" id="textbox_alert">
          <Alert.Heading>Warning!</Alert.Heading>
          <p>
            Please verify your email id to have full access.
          </p>
        </Alert> */}
        
      </React.Fragment>
    )
  }
}