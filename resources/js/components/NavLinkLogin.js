import React, { Component } from 'react';
import './NavLinkLogin.css';
import { Link } from 'react-router-dom';

export default class NavLinkLogin extends Component {
  



  render() {
    $('.nav-link').on('click',function() {
      $('.navbar-collapse').collapse('hide');
    });

    
    return (
      <React.Fragment>
        <div className="unpain__navbar__sec nnav__trans__back">
          <nav class="navbar navbar-expand-lg log__cusnavimain">
            <div className="nnav__kkwrap">
            <Link to="/" class="navbar-brand lognavi__aa">
              Song Library
            </Link>
            
            <div class="flex1"></div>
            <button class="navbar-toggler logg__btnnav" type="button" data-toggle="collapse" data-target="#navbarUnpaid" aria-controls="navbarUnpaid" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            </div>

            <div class="collapse navbar-collapse just__flexend mr-5 logNav llogihhb_3" id="navbarUnpaid">
              <ul class="navbar-nav nnavul__nav">
                <li class="nav-item">
                  <Link to="AllSong" class="nav-link">All Songs</Link>
                </li>
                <li class="nav-item">
                  <Link to="AllAlbum" class="nav-link">All Albums</Link>
                </li>
                {/* <li class="nav-item active">
                  <Link to="/User-profile" class="nav-link">User</Link>
                </li> */}
                
                <li class="nav-item dropdown">
                
                  <span class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="far fa-user-circle"></i> username
                  </span>
                
                  <div class="dropdown-menu user__pro__unpaid" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Logout</a>
                  </div>
                </li>
              </ul>
            </div>
          </nav>
        </div>
        
      </React.Fragment>
    )
  }
}
