import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './JoinUsToday.css';

export default class JoinUsToday extends Component {
  render() {
    return (
      <React.Fragment>
          <div className="joinus__today__section">
            <div className="container">
            <div className="row">
                <div className="col-md-6 m-auto">
                <div className="joinus__box">
                    <div className="joinus__heading">
                        <h3>It’s time to start <br /> investing in yourself</h3>
                        
                    </div>
                    
                    <div className="joinus__button__box">
                        <Link to="#">
                            <button className="btn joinus__btn">Get Your Song List</button>
                        </Link>
                    </div>
                    </div>
                </div>
            </div>
            </div>
          </div>
        
      </React.Fragment>
    )
  }
}
