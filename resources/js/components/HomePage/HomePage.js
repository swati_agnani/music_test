import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import BannerHome from './BannerHome';
import PopularSong from './PopularSong';
import PopularAlbum from './PopularAlbum';
import TypeList from './TypeList';
import JoinUsToday from './JoinUsToday';

export default class HomePage extends Component {

    componentDidMount() {
        window.scrollTo(0, 0)
    }

    render() {
       
        return (
            <React.Fragment>
                {/* {login_div} */}
                <BannerHome />
                <PopularSong />
                <PopularAlbum />
                <TypeList />
                <JoinUsToday />
            </React.Fragment>
        );
    }
}