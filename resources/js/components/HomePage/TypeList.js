import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './TypeList.css';

export default class TypeList extends Component {
    constructor () {
        super()
        this.state = {
          types: [],
        }
    }

    componentDidMount () {
        axios.get('/TypeHome').then(response => {
            this.setState({
                types: response.data
            })
        })
    }
  render() {
    const { types } = this.state
    return (
      <React.Fragment>
          
                <div className="courselist-mainwrap">
                   
                    <div className="courselist-div">
                        <div className="container">
                            <h2>Discover Songs</h2>
                            <div className="row mt-5">
                            {/* Course 1 */}
                            {types.map(type=>(
                            <div className="col-md-2 col-sm-4">
                                <Link to="#">
                                    <div className="video-list-box">
                                        <div className="video-list-header_1">
                                            <div className="video-list-img">
                                                <img src={type.image} alt="course" />
                                            </div>
                                        </div>
                                        
                                    </div>
                                </Link>
                            </div>
                            ))}
                            <div className="col-md-10 col-sm-6 hidden-xs"></div>
                            <div className="col-md-2 col-sm-6 col-xs-12 text-right">
                                <Link to="/All-Courses" className="btn viewall__btn">VIEW ALL</Link>
                            </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                {/* <Route exact path="/video/:id" component={VideoPlay} /> */}
                
            
      </React.Fragment>
    )
  }
}
