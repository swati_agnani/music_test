import React, { Component } from 'react';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import './BannerHome.css';

export default class BannerHome extends Component {
  constructor () {
    super()
    this.state = {
      results: '',
    }
    this.handleKeyPress = this.handleKeyPress.bind(this);
  }
  // componentDidMount() {
  //   this.handleKeyPress();
  // }
  handleKeyPress(event){
    var txt = document.getElementById("serach_field").value; 
    document.getElementById('result_div').style.display = "block";
    $.ajax({
      type: 'get',
      url: '/fetchResult/'+txt,
      success: function(data) {
          this.setState({ results: data })
      }.bind(this)
    });
  }

  render() {
    
    const { results } = this.state
    return (
        <React.Fragment>
            <div className="heroimg-div">
                <img src="images/Music-banner.jpg" alt="Banner" className="heroimg-home"/>
                <div className="heroimg-text">
                    <h1>Song Library</h1>
                    <h5>Serach Your Fav Music</h5>
                    <div className="home__playbutton__box" data-toggle="modal" data-target="#demo__video">
                      <div className="home__banner__paly__button">
                        <button className="btn play-btn"><img src="images/wcmsq-376753.png" alt="button" /></button>
                      </div>
                      <div className="home__banner__paly__text">
                        <h6>Serach <span>Here</span></h6>
                      </div>
                    </div>
                </div>

              {/*modal*/}

              <div class="modal fade signin__modalfade" id="demo__video" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-backdrop="static">
                  <div class="modal-dialog signin__modalbox" role="document">
                      <div class="modal-content">
                      <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLongTitle">Serach Here</h5>
                          <button type="button" class="close modal__colose" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                          </button>
                      </div>
                      <div class="modal-body signin__body">
                            <input type="text" name="Serach_field" className="form-control" id="serach_field" placeholder="Example: Vaaste, etc" onKeyUp={this.handleKeyPress} />
                            <div className="result_div" id="result_div">
                              {ReactHtmlParser(results)}
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <input type="button" name="serach_btn" id="serach_btn" className="form-control btn viewall__btn" value="serach"/>
                      </div>
                      
                      </div>
                  </div>
              </div>

              {/*end Modl */}

            </div>
        </React.Fragment>
    )
    
  }
}
