import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './PopularSong.css';

export default class PopularAlbum extends Component {
    constructor () {
        super()
        this.state = {
          Albums: [],
        }
    }

    componentDidMount () {
        axios.get('/popularAlbums').then(response => {
            this.setState({
                Albums: response.data
            })
        })
    }
  render() {
    const { Albums } = this.state
    return (
        
      <React.Fragment>
          
                <div className="courselist-mainwrap">
                   
                    <div className="courselist-div">
                        <div className="container">
                            <h2>Popular List of Albums</h2>
                            <div className="row mt-5">
                            {/* Course 1 */}
                            {Albums.map(album=>(
                            <div className="col-md-2 col-sm-4">
                                <Link to="#">
                                    <div className="video-list-box">
                                        <div className="video-list-header">
                                            <div className="video-list-img">
                                                <img src={album.image} alt="course" />
                                            </div>
                                        </div>
                                        <div className="video-list-content">
                                            {/* <h6>Feb 6th</h6> */}
                                            <h4>{album.name}</h4>
                                        </div>
                                    </div>
                                </Link>
                            </div>
                            ))}
                            
                            <div className="col-md-10 col-sm-6 hidden-xs"></div>
                            <div className="col-md-2 col-sm-6 col-xs-12 text-right">
                                <Link to="/All-Courses" className="btn viewall__btn">VIEW ALL</Link>
                            </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                {/* <Route exact path="/video/:id" component={VideoPlay} /> */}
                
            
      </React.Fragment>
    )
  }
}
