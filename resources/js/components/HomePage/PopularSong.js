import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './PopularSong.css';

export default class PopularSong extends Component {
    constructor () {
        super()
        this.state = {
          Songs: [],
        }
    }

    componentDidMount () {
        axios.get('/PopularSongs').then(response => {
            this.setState({
                Songs: response.data
            })
        })
    }
  render() {
    const { Songs } = this.state
    return (
      <React.Fragment>
          
                <div className="courselist-mainwrap">
                   
                    <div className="courselist-div">
                        <div className="container">
                            <h2>Popular List of Songs</h2>
                            <div className="row mt-5">
                            {/* Course 1 */}
                            {Songs.map(song=>(
                            <div className="col-md-2 col-sm-4">
                                <Link to="#">
                                    <div className="video-list-box">
                                        <div className="video-list-header">
                                            <div className="video-list-img">
                                                <img src={song.image} alt="course" />
                                            </div>
                                        </div>
                                        <div className="video-list-content">
                                            {/* <h6>Feb 6th</h6> */}
                                            <h4>{song.name}</h4>
                                            <div className="trainer-name-box">
                                                <p>{song.singer}</p>
                                                {/*<p><span><i class="far fa-heart"></i></span>{song.views}</p>*/}
                                            </div>
                                        </div>
                                    </div>
                                </Link>
                            </div>
                            ))}
                            
                            <div className="col-md-10 col-sm-6 hidden-xs"></div>
                            <div className="col-md-2 col-sm-6 col-xs-12 text-right">
                                <Link to="/All-Courses" className="btn viewall__btn">VIEW ALL</Link>
                            </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                {/* <Route exact path="/video/:id" component={VideoPlay} /> */}
                
            
      </React.Fragment>
    )
  }
}
