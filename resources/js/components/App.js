import React, { Component } from 'react'
    import ReactDOM from 'react-dom'
    import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
    import NavLink from './NavLink';
    import NavLinkLogin from './NavLinkLogin';
    import HomePage from './HomePage/HomePage';
    import AllSong from './AllSong/AllSong';
    import AllAlbum from './AllAlbum/AllAlbum';
    import Footer from './Footer';

    class App extends Component {
      constructor () {
        super()
        this.state = {
          users: [], 
          loggedInUser: [],
        }
        let log_div = document.getElementById('test11');
    }
    componentDidMount () {
        axios.get('/loginUserinfo').then(response => {
            this.setState({
                users: response.data
            })
        })
        axios.get('/loginUser').then(response => {
            this.setState({
                loggedInUser: response.data
            })
        })
    }

      render () {
        const { users, loggedInUser } = this.state;
        var login_div;
        if (users.id) {
            login_div = <NavLinkLogin/>;
        } 
        else {
            login_div = <NavLink/>;
        }
        return (
          <React.Fragment>
            <Router>
              <div>
                {/* <NavLink />  */}
                {/* <NavLinkLogin /> */}
                {login_div}
                <Switch>
                  <Route exact path="/" component={HomePage} />
                  <Route exact path="/home" component={HomePage} />
                  <Route exact path="/AllSong" component={AllSong} />
                  <Route exact path="/AllAlbum" component={AllAlbum} />
                </Switch>
                <Footer />
              </div>
            </Router>
          </React.Fragment>
        )
      }
    }

    ReactDOM.render(<App />, document.getElementById('app'))