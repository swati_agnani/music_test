import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './NavLink.css';

export default class NavLink extends Component {
    
    render() {
        $(function() {
            $(document).click(function (event) {
              $('.navbar-collapse').collapse('hide');
            });
          });

        $('.nav-link').on('click',function() {
            $('.navbar-collapse').collapse('hide');
          });
        
        return (
            <React.Fragment>
                
                <div className="navigation__home">
                    <div className="container-fluid">
                        <nav className="navbar navbar-expand-lg navbar-light gen__mainnavi">
                        <Link to="/" className="navbar-brand" >
                            {/*<img src="images/songlibrary-logo-1200x627.png" alt="Logo" />*/}
                            SONG LIBRARY
                        </Link>
                        <div className="flex1"></div>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        
                            <div className="collapse navbar-collapse just__flexend gen__navi" id="navbarSupportedContent">
                                <ul className="navbar-nav link__home__navbar">
                                    <li className="nav-item nav__a1" >
                                        <Link to="AllSong">Songs</Link>
                                    </li>
                                    <li className="nav-item nav__a1" >
                                        <Link to="AllAlbum">Album</Link>
                                    </li>
                                    
                                    <li className="nav-item">
                                        <button type="button" className="btn red__btn mar_right15" data-toggle="modal" data-target="#signin">
                                            Sign In
                                        </button>
                                    </li>
                                    <li className="nav-item">
                                        <button type="button" className="btn red__btn" data-toggle="modal" data-target="#signup">
                                            Sign Up
                                        </button> 
                                        {/*<button type="button" className="btn red__btn">
                                            Sign Up
                                        </button>*/}
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            
                
            </React.Fragment>
        );
    }
}