import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import BannerHome from '../HomePage/BannerHome';
import SongList from './SongList';

export default class AllSong extends Component {

    componentDidMount() {
        window.scrollTo(0, 0)
    }

    render() {
       
        return (
            <React.Fragment>
                {/* {login_div} */}
                <BannerHome />
                <SongList />
            </React.Fragment>
        );
    }
}