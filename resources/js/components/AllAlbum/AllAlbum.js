import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import BannerHome from '../HomePage/BannerHome';
import AlbumList from './AlbumList';

export default class AllAlbum extends Component {

    componentDidMount() {
        window.scrollTo(0, 0)
    }

    render() {
       
        return (
            <React.Fragment>
                {/* {login_div} */}
                <BannerHome />
                <AlbumList />
            </React.Fragment>
        );
    }
}