<!DOCTYPE html>
    <html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Tasksman</title>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <!-- <link href="{{url('/css/customModal.css')}}" rel="stylesheet" type="text/css"> -->
    </head>
    <body>
    @if (session()->has('success'))
        <script>
            var msg = '{!! Session::get('success') !!}';
            var exist = '{{Session::has('success')}}';
            // alert(msg);
            // msg = JSON.parse(msg);
            // var arr = [];
            // for(var x in msg){
            //     arr.push(msg[x]);
            // }
            // var msg2 = arr[0];
            if(exist){
                setTimeout(function () { 
                    swal('Success',msg,"success");
                }, 1000);
            }
        </script>
        @endif
        @if (session()->has('errors'))
        <script>
            var msg = '{!! Session::get('errors') !!}';
            var exist = '{{Session::has('errors')}}';
            // alert(msg);
            msg = JSON.parse(msg);
            var arr = [];
            for(var x in msg){
                arr.push(msg[x]);
            }
            var msg2 = arr[0];
            if(exist){
                setTimeout(function () { 
                    swal('Error',msg2,"error");
                }, 1000);
            }
        </script>    
        @endif
        @if (session()->has('error'))
        <script>
            var msg = '{!! Session::get('error') !!}';
            var exist = '{{Session::has('error')}}';
            if(exist){
                setTimeout(function () { 
                    swal('Error',msg,"error");
                }, 1000);
            }
        </script>    
        @endif
        <div id="app"></div>

        <script src="{{ asset('js/app.js') }}"></script>

        <div class="modal fade signin__modalfade" id="signin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-backdrop="static">
            <div class="modal-dialog signin__modalbox" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Sign In</h5>
                    <button type="button" class="close modal__colose" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body signin__body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-10 m-auto signin__form__col">
                                <label for="email">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="form-control" name="email" value="" required autofocus autocomplete="false">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-10 m-auto signin__form__col">
                                <label for="password">{{ __('Password') }}</label>
                                <input id="password" type="password" class="form-control" name="password" required autocomplete="false">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-10 m-auto signin__form__col">
                                <button type="submit" class="btn btn__ligin">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer signin__modal__footer">
                    
                </div>
                </div>
            </div>
        </div>
        <div class="modal fade signin__modalfade" id="signup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-backdrop="static">
            <div class="modal-dialog signin__modalbox" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Sign Up</h5>
                        <button type="button" class="close modal__colose" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="{{ route('register') }}" id="registerFrom">
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-10 m-auto signin__form__col">
                                    <label for="name">{{ __('Name') }}</label>
                                    <input id="name" type="text" class="form-control" name="name" value="" required autofocus autocomplete="false">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-10 m-auto signin__form__col">
                                <label for="email">{{ __('E-Mail Address') }}</label>
                                    <input id="email" type="email" class="form-control" name="email" value="" required autocomplete="false">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-10 m-auto signin__form__col">
                                    <label for="password">{{ __('Password') }}</label>
                                    <input id="password" type="password" class="form-control" name="password" required autocomplete="false">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-10 m-auto signin__form__col">
                                    <label for="password-confirm">{{ __('Confirm Password') }}</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="false">
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-10 m-auto signin__form__col">
                                    <button type="submit" class="btn btn__ligin" id="registerBtn">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer signin__modal__footer">  
                    </div>
                </div>
            </div>
        </div>

    </body>
    </html>