<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/PopularSongs','SongController@PopularSongs');

Route::get('/popularAlbums', 'AlbumController@popularAlbums');

Route::get('/TypeHome', 'TypeController@TypeHome');

Route::get('/fetchResult/{id}', 'SongController@fetchResult');

Route::get('/allSongs', 'SongController@allSongs');

Route::get('/allAlbum', 'AlbumController@allAlbum');

Route::get('/logout', 'UsersController@logout');

Route::get('/loginUserinfo', 'UsersController@loginUserinfo');

Route::get('/loginUser', 'UsersController@loginUser');

Route::get( '/{path?}', function(){
    return view('app');
} )->where('path', '.*');