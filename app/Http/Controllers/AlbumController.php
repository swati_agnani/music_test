<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Album;

class AlbumController extends Controller
{
    public function popularAlbums(){
        $albums = Album::take(6)->get();
        return response()->json($albums);
    }
    
    public function allAlbum(){
        $albums = Album::get();
        return response()->json($albums);
    }
}
