<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;

class TypeController extends Controller
{
    public function TypeHome(){
        $types = Type::take(6)->get();
        return response()->json($types);
    }
}
