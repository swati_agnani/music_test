<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    public function loginUserinfo(){
        $users = User::where('id', Auth::id())->first();
        return response()->json($users);
    }

    public function loginUser(){
        $loggedInUser = User::select('id')->where('id', '=', Auth::id())->count();
        return response()->json($loggedInUser);
    }

    public function logout() {
        //logout user
        // dd(Auth::id());
        auth()->logout();
        // redirect to homepage
        return redirect('/');
    }
}
