<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Song;
use App\Album;
use DB;

class SongController extends Controller
{
    public function PopularSongs(){
        $songs = Song::join('album_songs','album_songs.song_id','songs.id')->join('albums','albums.id','album_songs.album_id')->orderBy('songs.views', 'desc')->take(6)->get();
        return response()->json($songs);
    }
    public function fetchResult($id){
        $songs_serach = DB::table('songs')->select('name', 'id')->where('name', 'like', '%'.$id.'%')->get();
        $str ='';
        foreach($songs_serach as $key =>$value){
            $str .= '<Link to="#">'.$value->name.'</Link><br />';
        }

        return response($str);
    }
    public function allSongs(){
        $songs = Song::join('album_songs','album_songs.song_id','songs.id')->join('albums','albums.id','album_songs.album_id')->orderBy('songs.views', 'desc')->get();
        return response()->json($songs);
    }
}
